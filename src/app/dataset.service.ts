import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpParams, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TileModel } from './dashboard/tiles/common-tile/tile.model';

@Injectable({
  providedIn: 'root'
})
export class DatasetService {

  confUrl = 'http://localhost:8000/conf';
  datasetUrl = 'http://localhost:8000/dataset';

  constructor(private http: HttpClient) { }

  getDataset() {
    const params = new HttpParams();
    const options = {
      params,
      reportProgress: false,
    };
    return this.http.get<string>(this.datasetUrl, options);
  }

  deleteDataset() {
    const params = new HttpParams();
    const options = {
      params,
      reportProgress: false,
    };
    return this.http.delete<string>(this.datasetUrl, options);
  }

  uploadDataset(file: File): Observable<HttpEvent<any>> {
    const formData = new FormData();
    formData.append('file', file);

    const params = new HttpParams();
    const options = {
      params,
      reportProgress: true,
    };

    const req = new HttpRequest('POST', this.datasetUrl, formData, options);
    return this.http.request(req);
  }

  uploadConfiguration(conf) {
    console.log(`my configuration: ${conf}`);

    const formData = new FormData();
    const params = new HttpParams();
    const options = {
      params,
      reportProgress: false,
    };

    const req = new HttpRequest('POST', this.confUrl, formData, options);
    return this.http.request(req);
  }

  loadConfiguration() {
    const params = new HttpParams();
    const options = {
      params,
      reportProgress: false,
    };
    return this.http.get<TileModel[]>(this.confUrl, options);
  }
}
