import { Component, OnInit, OnChanges, Input, InjectionToken, Injector, ViewChildren, Inject, QueryList } from '@angular/core';
import { DataframeService } from '../../dataframe.service';
import { Overlay, OverlayRef, CdkOverlayOrigin } from '@angular/cdk/overlay';
import { ComponentPortal, PortalInjector } from '@angular/cdk/portal';
import { DatasetService } from 'src/app/dataset.service';

export const PORTAL_DATA = new InjectionToken<{}>('PORTAL_DATA');

/**
 * Object to be used by [[compareFn]] for ordering infos consistently.
 */
const INFOS_ORDER = {
  'Column name': 1,
  count: 2,
  unique: 3,
  top: 4,
  freq: 5,
  mean: 6,
  std: 7,
  min: 8,
  '25%': 9,
  '50%': 10,
  '75%': 11,
  max: 12
};

@Component({
  selector: 'app-head',
  templateUrl: './head.component.html',
  styleUrls: ['./head.component.scss']
})
export class HeadComponent implements OnInit, OnChanges {
  head: any;
  cols: string[];
  rowsCount: number;
  colsCount: number;
  size: string;
  @Input() dataset: string;
  overlayRef: OverlayRef;
  displaySamples = true;

  @ViewChildren(CdkOverlayOrigin) overlayOrigins: QueryList<CdkOverlayOrigin>;

  constructor(
    public dataframeService: DataframeService,
    public datasetService: DatasetService,
    public overlay: Overlay,
    private injector: Injector
  ) { }

  ngOnInit() {
    /* On init, ask backend for previous file presence */
    this.datasetService.getDataset()
      .subscribe((data: any) => {
        this.dataset = data.name;
        this.rowsCount = data.rows;
        this.colsCount = data.cols;
        this.size = data.size;

        this.dataframeService.getHead()
          .subscribe((head: any) => {
            this.head = head;
            console.log(this.head);
          });
        this.dataframeService.getCols()
          .subscribe((cols: string[]) => {
            this.cols = cols;
          });
      });
  }

  ngOnChanges() { // ngOnChanges is trigger for property-bound changes
    if (this.dataset) { // new CSV uploaded via button
      console.log(`New dataset selected: ${this.dataset}.`);

      this.datasetService.getDataset()
        .subscribe((data: any) => {
          this.rowsCount = data.rows;
          this.colsCount = data.cols;
          this.size = data.size;

          this.dataframeService.getHead()
            .subscribe((head: any) => {
              this.head = head;
            });
          this.dataframeService.getCols()
            .subscribe((cols: string[]) => {
              this.cols = cols;
            });
        });
    }
  }

  public displayInfos(col) {
    /* Create a default overlay before receiving the data */
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().connectedTo(
        this.overlayOrigins.find((_, index) => index === this.cols.findIndex(c => c === col)).elementRef,
        { originX: 'end', originY: 'center' },
        { overlayX: 'start', overlayY: 'center' } )
    });

    let columnInfosPortal = new ComponentPortal(
      // tslint:disable-next-line: no-use-before-declare
      ColumnInfosComponent,
      null,
      this.createInjector(null) // Empty injector
    );
    this.overlayRef.attach(columnInfosPortal);

    /* Async call to backend */
    this.dataframeService.getInfos(col)
      .subscribe((data: any) => {
        this.overlayRef.detach();
        columnInfosPortal = new ComponentPortal(
          // tslint:disable-next-line: no-use-before-declare
          ColumnInfosComponent,
          null,
          this.createInjector(data) // This time we really inject our data
        );
        try {
          this.overlayRef.attach(columnInfosPortal);
        } catch (err) {
          console.log('Overlay was disposed before receiving data from backend.');
        }
      });
  }

  public hideInfos() {
    this.overlayRef.detach();
    this.overlayRef.dispose();
  }

  createInjector(dataToPass): PortalInjector {
    /* Returns an injector for token PORTAL_DATA, used later for creating Portal */
    const injectorTokens = new WeakMap();
    injectorTokens.set(PORTAL_DATA, dataToPass);
    return new PortalInjector(this.injector, injectorTokens);
  }
}

/**
 * This component is dinamically created to display infos within an overlay
 */
@Component({
  templateUrl: './column-infos/column-infos.component.html',
  styleUrls: ['./column-infos/column-infos.component.scss']
})
export class ColumnInfosComponent {

  constructor(
    public dataframeService: DataframeService,
    @Inject(PORTAL_DATA) public componentData: any
  ) { }

  /* Comparison function for ordering infos */
  public compareFn(a: KeyValue<string, any>, b: KeyValue<string, any>): number {
    if (a.key === b.key) {
      return 0;
    }
    return INFOS_ORDER[a.key] < INFOS_ORDER[b.key] ? -1 : 1;
  }
}

/**
 * Simple KeyValue interface implementation.
 */
export interface KeyValue<K, V> {
  key: K;
  value: V;
}


