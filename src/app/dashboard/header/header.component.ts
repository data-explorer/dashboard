import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DatasetService } from 'src/app/dataset.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { IpcRenderer } from 'electron';
import { DataframeService } from 'src/app/dataframe.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  widthProperty: any;
  loaded = true;
  @Output() eventDataset = new EventEmitter<string>(); // String (filename) Emitted when file is uploaded
  private ipc: IpcRenderer;

  constructor(
    public datasetService: DatasetService,
    public dataframeService: DataframeService
  ) {
    if ((<any>window).require) {
      try {
        this.ipc = (<any>window).require('electron').ipcRenderer;
      } catch (e) {
        throw e;
      }
    } else {
      console.log('App not running inside ELectron !');
    }
  }

  ngOnInit() { }

  csvInputChange(fileInputEvent: any) {
    const files: FileList = fileInputEvent.target.files;
    this.uploadFile(files);
  }

  uploadFile(files: FileList) {
    if (files.length === 0) {
      return;
    }
    const file: File = files[0];

    this.datasetService.uploadDataset(file)
      .subscribe(
        event => {
          if (event.type === HttpEventType.UploadProgress) { // Still uploading
            if (this.loaded) {
              this.loaded = false;
            }
            const percent = Math.round(100 * event.loaded / event.total);
            console.log(`${file.name} upload: ${percent}%.`);
            this.widthProperty = { width: `${percent}%` };
          } else if (event instanceof HttpResponse) { // Upload finished
            this.eventDataset.emit(file.name);
            setTimeout(() => { this.loaded = true; }, 1000);
          }
        },
        (err) => {
          console.log('Upload error: ', err);
        },
        () => {
          console.log('Upload done.');
        }
      );
  }

  closeApp() {
    console.log('User clicked on close button.');
    this.ipc.send('closeApp');
  }

  maximizeApp() {
    this.ipc.send('maximizeApp');
  }

  minimizeApp() {
    this.ipc.send('minimizeApp');
  }

  switchTheme() {
    if (document.body.classList.contains('light')) {
      this.dataframeService.postStyle({style: 'dark'})
        .subscribe((res) => {
          console.log('Backend plot style switched to dark.');
          console.log(res);
          document.body.classList.add('dark');
          document.body.classList.remove('light');
        });
    } else if (document.body.classList.contains('dark')) {
      this.dataframeService.postStyle({style: 'light'})
        .subscribe((res) => {
          console.log('Backend plot style switched to light.');
          console.log(res);
          document.body.classList.add('light');
          document.body.classList.remove('dark');
        });
    }
  }
}
