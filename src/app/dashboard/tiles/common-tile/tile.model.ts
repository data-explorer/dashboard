import { Tiles } from 'src/app/shared/tiles.enum';

export class TileModel {
  type: Tiles;
  col?: string;
  x?: string;
  y?: string;
  yMax?: number;
  yMin?: number;
  bins?: number;
  marker?: string;
  color?: string;
  line?: string;
  imgSrc?: string;
  selected?: string[];
}
