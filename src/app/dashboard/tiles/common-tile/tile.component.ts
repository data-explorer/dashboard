import { Input } from '@angular/core';
import { TileModel } from './tile.model';

export declare abstract class Tile {
  abstract getConfiguration(): any;
}
