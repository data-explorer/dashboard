import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistogramTileComponent } from './histogram-tile.component';

describe('HistogramTileComponent', () => {
  let component: HistogramTileComponent;
  let fixture: ComponentFixture<HistogramTileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistogramTileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistogramTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
