import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { DataframeService } from '../../../dataframe.service';
import { Tile } from '../common-tile/tile.component';
import { TileModel } from '../common-tile/tile.model';
import { Tiles } from 'src/app/shared/tiles.enum';

@Component({
  selector: 'app-histogram-tile',
  templateUrl: './histogram-tile.component.html',
  styleUrls: ['./histogram-tile.component.scss', '../common-tile/tile.common.scss']
})
export class HistogramTileComponent implements OnInit, Tile {
  @Input() conf: TileModel;
  @Output() deleteEvent = new EventEmitter<TileModel>();
  cols: string[];
  backgroundImageProperty: any;
  paramsVisible = false;
  imageToShow: any;
  isImageLoading: boolean;

  /* Configuration variables */
  x: string;
  bins = 0;

  constructor(public dataframeService: DataframeService) { }

  ngOnInit() {
    this.setConfiguration();

    this.dataframeService.getCols()
      .subscribe((data: string[]) => {
        this.cols = data;
      });

    this.getHist(this.x, this.bins);
  }

  public updateColumn(event) {
    if (event.isUserInput) {
      this.x = event.source.value;
      this.getHist(this.x, this.bins);
    }
  }

  public updateBins(event) {
    if (true) {
      this.bins = event.target.value;
      if (this.x) {
        this.getHist(this.x, this.bins);
      }
    }
  }

  createImageFromBlob(image: Blob) {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      this.imageToShow = reader.result;
      this.backgroundImageProperty = { 'background-image': `url(${this.imageToShow})` };
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
  }

  getHist(x: string, bins: number) {
    this.isImageLoading = true;

    this.dataframeService.getHist(x, bins)
      .subscribe((data) => {
        this.createImageFromBlob(data);
        this.isImageLoading = false;
      }, error => {
        this.isImageLoading = false;
        console.log(error);
      });
  }

  public getConfiguration() {
    const config = {
      type: Tiles.Histogram,
      x: this.x,
      bins: this.bins
    };
    return config;
  }

  public setConfiguration() {
    this.x = this.conf.x;
    this.bins = this.conf.bins;
    console.log(`Creating histogram with parameters: ${this.x};${this.bins};`);
  }

  public emitDeleteEvent() {
    this.deleteEvent.emit(this.conf);
  }
}
