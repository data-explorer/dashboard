import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { Tiles } from '../../../shared/tiles.enum'
import { Tile } from '../common-tile/tile.component';

@Component({
  selector: 'app-empty-tile',
  templateUrl: './empty-tile.component.html',
  styleUrls: ['./empty-tile.component.scss', '../common-tile/tile.common.scss']
})
export class EmptyTileComponent implements OnInit, Tile {
  tilesEnum = Tiles;
   // In order to use it in template
  @Output() eventAddTile = new EventEmitter<Tiles>();
  keys = (tiles) => Object.keys(tiles).filter((key) => !isNaN(Number(tiles[key])));

  constructor() { }

  ngOnInit() {
  }

  public addTile(event) {
    if (event.isUserInput) {
      console.log(`${Tiles[event.source.value]} tile added.`);
      this.eventAddTile.emit(event.source.value);
    }
  }

  public getConfiguration() {
    return { type: Tiles.Empty };
  }
}
