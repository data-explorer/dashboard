import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { Tiles } from '../../shared/tiles.enum';
import { Tile } from './common-tile/tile.component';
import { DatasetService } from 'src/app/dataset.service';
import { TileModel } from './common-tile/tile.model';

@Component({
  selector: 'app-tiles',
  templateUrl: './tiles.component.html',
  styleUrls: ['./tiles.component.scss']
})
export class TilesComponent implements OnInit {
  public tilesList: TileModel[] = [];
  public tilesEnum = Tiles;

  @ViewChildren('tile') public tiles: QueryList<Tile>;

  constructor(public datasetService: DatasetService) { }

  ngOnInit() {
    const emptyTile = new TileModel();
    emptyTile.type = Tiles.Empty;
    this.tilesList.push(emptyTile);
    this.loadTiles();
  }

  public saveTiles() {
    const tilesConf = [];
    this.tiles.forEach((t) => {
      tilesConf.push(t.getConfiguration());
    });
    console.log(JSON.stringify(tilesConf));
    this.datasetService.uploadConfiguration(tilesConf)
      .subscribe(
        (val) => {
          console.log('POST succesful, value returned: ', val);
        },
        () => {
          console.log('Upload done.');
          console.log(`Saved configuration : ${tilesConf}`);
        }
      );
  }

  public loadTiles() {
    this.datasetService.loadConfiguration()
      .subscribe(
        (res) => {
          this.tilesList = res as TileModel[];
          console.log('Successfully received saved configuration.');
          console.log(this.tilesList);
        }
      );
  }

  public addTile(event) {
    const newTile = new TileModel();
    newTile.type = event;
    this.tilesList.splice(1, 0, newTile); // Insert at index 1
  }

  public deleteTile(tileModel: TileModel) {
    this.tilesList.splice(this.tilesList.indexOf(tileModel), 1);
  }
}
