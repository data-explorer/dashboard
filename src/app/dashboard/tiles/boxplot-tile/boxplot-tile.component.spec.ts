import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxplotTileComponent } from './boxplot-tile.component';

describe('BoxplotTileComponent', () => {
  let component: BoxplotTileComponent;
  let fixture: ComponentFixture<BoxplotTileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoxplotTileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxplotTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
