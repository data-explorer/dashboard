import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DataframeService } from 'src/app/dataframe.service';
import { HttpUrlEncodingCodec } from '@angular/common/http';
import { Tile } from '../common-tile/tile.component';
import { Tiles } from 'src/app/shared/tiles.enum';
import { TileModel } from '../common-tile/tile.model';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-boxplot-tile',
  templateUrl: './boxplot-tile.component.html',
  styleUrls: ['./boxplot-tile.component.scss', '../common-tile/tile.common.scss']
})
export class BoxplotTileComponent implements OnInit, Tile {
  @Input() conf: TileModel;
  @Output() deleteEvent = new EventEmitter<TileModel>();
  cols: string[];
  categories: string[];
  categoriesCtl = new FormControl();
  backgroundImageProperty: any = null;
  codec = HttpUrlEncodingCodec;
  paramsVisible = false;
  imageToShow: any;
  isImageLoading: boolean;

  /* Configuration variables */
  x: string;
  y: string;
  selected: string[] = [];
  yMin = 0;
  yMax = 0;

  constructor(public dataframeService: DataframeService) { }

  ngOnInit() {
    this.setConfiguration();

    this.dataframeService.getCols()
      .subscribe((data: string[]) => {
        this.cols = data;
      });

    this.getBoxplot(this.x, this.y, this.selected, this.yMin, this.yMax);
  }

  public updateX(event) {
    if (event.isUserInput) {
      this.x = event.source.value;
      this.dataframeService.getCategories(this.x)
        .subscribe((data: string[]) => {
          this.categories = data;
        });
    }
  }

  public updateY(event) {
    if (event.isUserInput) {
      this.y = event.source.value;
      this.getBoxplot(this.x, this.y, this.selected, this.yMin, this.yMax);
    }
  }

  public updateSelected(event) {
    if (!this.selected.includes(event.source.value)) {
      this.selected.push(event.source.value)
    } else {
      const index = this.selected.indexOf(event.source.value, 0);
      if (index > -1) {
        this.selected.splice(index, 1);
      }
    }
  }

  public updateYMin(event) {
    if (true) {
      this.yMin = event.target.value;
      if (this.yMin) {
        this.getBoxplot(this.x, this.y, this.selected, this.yMin, this.yMax);
      }
    }
  }

  public updateYMax(event) {
    if (true) {
      this.yMax = event.target.value;
      if (this.yMax) {
        this.getBoxplot(this.x, this.y, this.selected, this.yMin, this.yMax);
      }
    }
  }

  createImageFromBlob(image: Blob) {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      this.imageToShow = reader.result;
      this.backgroundImageProperty = { 'background-image': `url(${this.imageToShow})` };
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
  }

  getBoxplot(x: string, y: string, selected: string[], yMin: number, yMax: number) {
    this.isImageLoading = true;

    this.dataframeService.getBoxplot(x, y, selected, yMin, yMax)
      .subscribe((data: Blob) => {
        this.createImageFromBlob(data);
        this.isImageLoading = false;
      }, error => {
        this.isImageLoading = false;
        console.log(error);
      });
  }

  public getConfiguration() {
    const config = {
      type: Tiles.Boxplot,
      x: this.x,
      y: this.y,
      yMin: this.yMin,
      yMax: this.yMax,
      selected: this.selected,
    };
    return config;
  }

  public setConfiguration() {
    this.x = this.conf.x;
    this.y = this.conf.y;
    this.yMin = this.conf.yMin;
    this.yMax = this.conf.yMax;
    this.selected = this.conf.selected;
    console.log(`Creating Boxplot with parameters: ${this.x};${this.y};${this.yMin};${this.yMax};${this.selected};`);
  }

  public emitDeleteEvent() {
    this.deleteEvent.emit(this.conf);
  }
}
