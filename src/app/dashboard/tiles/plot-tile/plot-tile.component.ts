import { Component, OnInit, InjectionToken, ViewChild, Injector, Inject, Output, EventEmitter, ComponentRef, Input } from '@angular/core';
import { DataframeService } from 'src/app/dataframe.service';
import { CdkOverlayOrigin, OverlayRef, Overlay } from '@angular/cdk/overlay';
import { ComponentPortal, PortalInjector } from '@angular/cdk/portal';
import { Tile } from '../common-tile/tile.component';
import { TileModel } from '../common-tile/tile.model';
import { Tiles } from 'src/app/shared/tiles.enum';

export let STYLE_DATA = new InjectionToken<{}>('STYLE_DATA');
const STYLE_VALUES = {
  Colors: {
    Red: '#e06c75',
    Green: '#98c379',
    Yellow: '#d19a66',
    Blue: '#61afef',
    Purple: '#c678dd',
    Cyan: '#56b6c2',
    Grey: '#abb2bf'
  },

  Lines: {
    Dashed: 'dashed',
    Solid: 'solid',
    Dashdot: 'dashdot',
    Dotted: 'dotted',
    None: 'none',
  },

  Markers: {
    Point: '.',
    Triangle: 'v',
    Circle: 'o',
    Square: 's',
    Plus: '+',
    Star: '*',
    Diamond: 'D',
    None: 'none'
  }
};

@Component({
  selector: 'app-plot-tile',
  templateUrl: './plot-tile.component.html',
  styleUrls: ['./plot-tile.component.scss', '../common-tile/tile.common.scss']
})
export class PlotTileComponent implements OnInit, Tile {
  @Input() conf: TileModel;
  @Output() deleteEvent = new EventEmitter<TileModel>();
  cols: string[];
  backgroundImageProperty: any = null;
  overlayRef: OverlayRef = null;
  paramsVisible = false;
  imageToShow: any;
  isImageLoading: boolean;

  /* Configuration variables */
  x: string = null;
  y: string = null;
  color: string = STYLE_VALUES.Colors.Blue;
  line: string = STYLE_VALUES.Lines.None;
  marker: string = STYLE_VALUES.Markers.Star;

  @ViewChild(CdkOverlayOrigin) overlayOrigin: CdkOverlayOrigin;

  constructor(
    public dataframeService: DataframeService,
    public overlay: Overlay,
    private injector: Injector
  ) { }

  ngOnInit() {
    this.setConfiguration();

    this.dataframeService.getCols()
      .subscribe((data: string[]) => {
        this.cols = data;
      });

    this.getPlot(this.x, this.y, this.color, this.line, this.marker);
  }

  public updateX(event) {
    if (event.isUserInput) {
      this.x = event.source.value;
      this.getPlot(this.x, this.y, this.color, this.line, this.marker);
    }
  }

  public updateY(event) {
    if (event.isUserInput) {
      this.y = event.source.value;
      this.getPlot(this.x, this.y, this.color, this.line, this.marker);
    }
  }

  createImageFromBlob(image: Blob) {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      this.imageToShow = reader.result;
      this.backgroundImageProperty = { 'background-image': `url(${this.imageToShow})` };
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
  }

  getPlot(x: string, y: string, color: string, linestyle: string, marker: string) {
    this.isImageLoading = true;

    this.dataframeService.getPlot(x, y, color, linestyle, marker)
      .subscribe((data) => {
        this.createImageFromBlob(data);
        this.isImageLoading = false;
      }, error => {
        this.isImageLoading = false;
        console.log(error);
      });
  }

  public displayStyleOverlay(style: string) {
    const dataToPass: any = STYLE_VALUES;
    dataToPass.CurrentStyle = style;
    this.overlayRef = this.overlay.create({
      hasBackdrop: true,
      positionStrategy: this.overlay.position().connectedTo(
        this.overlayOrigin.elementRef,
        { originX: 'end', originY: 'center' },
        { overlayX: 'start', overlayY: 'center' } )
    });

    const stylePortal = new ComponentPortal(
      // tslint:disable-next-line: no-use-before-declare
      StyleOverlayComponent,
      null,
      this.createInjector(dataToPass)
    );

    try {
      const componentRef: ComponentRef<StyleOverlayComponent> = this.overlayRef.attach(stylePortal);
      this.overlayRef.backdropClick().subscribe(() => {
        this.hideStyleOverlay();
      });
      componentRef.instance.styleEvent.subscribe((event) => {
        switch (style) {
          case 'colors': this.setPlotColor(event); break;
          case 'lines': this.setPlotLine(event); break;
          case 'markers': this.setPlotMarker(event); break;
        }
        this.hideStyleOverlay();
      });
    } catch (err) {
      console.log(err);
    }
  }

  public hideStyleOverlay() {
    this.overlayRef.detach();
    this.overlayRef.dispose();
  }

  public setPlotColor(event) {
    this.color = STYLE_VALUES.Colors[event];
    this.getPlot(this.x, this.y, this.color, this.line, this.marker);
  }

  public setPlotLine(event) {
    this.line = STYLE_VALUES.Lines[event];
    this.getPlot(this.x, this.y, this.color, this.line, this.marker);
  }

  public setPlotMarker(event) {
    this.marker = STYLE_VALUES.Markers[event];
    this.getPlot(this.x, this.y, this.color, this.line, this.marker);
  }

  createInjector(dataToPass): PortalInjector {
    const injectorTokens = new WeakMap();
    injectorTokens.set(STYLE_DATA, dataToPass);
    return new PortalInjector(this.injector, injectorTokens);
  }

  public getConfiguration() {
    const config = {
      type: Tiles.Plot,
      x: this.x,
      y: this.y,
      color: this.color,
      line: this.line,
      marker: this.marker
    };
    return config;
  }

  public setConfiguration() {
    this.x = this.conf.x;
    this.y = this.conf.y;
    this.color = this.conf.color;
    this.line = this.conf.line;
    this.marker = this.conf.marker;
    console.log(`Created plot with parameters: ${this.x};${this.y};${this.color};${this.line};${this.marker};`);
  }

  public emitDeleteEvent() {
    this.deleteEvent.emit(this.conf);
  }
}

@Component({
  templateUrl: './style-overlay/style-overlay.component.html',
  styleUrls: ['./style-overlay/style-overlay.component.scss']
})
export class StyleOverlayComponent {
  @Output() styleEvent: EventEmitter<string> = new EventEmitter<string>();
  style: string;

  constructor(@Inject(STYLE_DATA) public styles: any) {
    this.style = styles.CurrentStyle;
    console.log(`${this.style} overlay created.`);
  }

  emitSyleEvent(style: string) {
    console.log(`Emitting style event: ${style}.`);
    this.styleEvent.emit(style);
  }
}

export interface KeyValue<K, V> {
  key: K;
  value: V;
}
