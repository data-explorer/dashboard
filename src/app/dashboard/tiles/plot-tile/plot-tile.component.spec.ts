import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlotTileComponent } from './plot-tile.component';

describe('PlotTileComponent', () => {
  let component: PlotTileComponent;
  let fixture: ComponentFixture<PlotTileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlotTileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlotTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
