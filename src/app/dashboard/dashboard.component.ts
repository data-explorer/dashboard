import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public datasetEvent: Event;

  constructor() { }

  ngOnInit() {
    document.body.classList.add('light');
  }

  refreshHead(event: Event) {
    this.datasetEvent = event;
  }
}
