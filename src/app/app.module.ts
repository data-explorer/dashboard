import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeadComponent, ColumnInfosComponent } from './dashboard/head/head.component';
import { HeaderComponent } from './dashboard/header/header.component';
import { TilesComponent } from './dashboard/tiles/tiles.component';
import { EmptyTileComponent } from './dashboard/tiles/empty-tile/empty-tile.component';
import { HistogramTileComponent } from './dashboard/tiles/histogram-tile/histogram-tile.component';
import { OverlayContainer, OverlayModule } from '@angular/cdk/overlay';
import { MatSelectModule, MatFormFieldModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PlotTileComponent } from './dashboard/tiles/plot-tile/plot-tile.component';
import { StyleOverlayComponent } from './dashboard/tiles/plot-tile/plot-tile.component';
import { BoxplotTileComponent } from './dashboard/tiles/boxplot-tile/boxplot-tile.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    HeadComponent,
    HeaderComponent,
    TilesComponent,
    EmptyTileComponent,
    HistogramTileComponent,
    ColumnInfosComponent,
    PlotTileComponent,
    StyleOverlayComponent,
    BoxplotTileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatSelectModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatInputModule,
    OverlayModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    ColumnInfosComponent,
    StyleOverlayComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(overlayContainer: OverlayContainer) {
    overlayContainer.getContainerElement().classList.add('custom-theme');
  }
}
