export enum Tiles {
    Empty,
    Histogram,
    Plot,
    Boxplot
}
