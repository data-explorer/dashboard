import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataframeService {
  histUrl = 'http://localhost:8000/hist';
  plotUrl = 'http://localhost:8000/plot';
  boxplotUrl = 'http://localhost:8000/boxplot';
  colsUrl = 'http://localhost:8000/cols';
  categoriesUrl = 'http://localhost:8000/categories';
  headUrl = 'http://localhost:8000/head';
  infosUrl = 'http://localhost:8000/infos';
  styleUrl = 'http://localhost:8000/style';

  constructor(private http: HttpClient) { }

  getCols() {
    return this.http.get<string[]>(this.colsUrl);
  }

  getHead() {
    return this.http.get(this.headUrl);
  }

  getHist(x, bins): Observable<Blob> {
    return this.http.get(`${this.histUrl}?col=${x}&bins=${bins}`, {responseType: 'blob'});
  }

  getPlot(x, y, color, linestyle, marker): Observable<Blob> {
    let url = `${this.plotUrl}?x=${x}&y=${y}`;
    url += `&color=${color.replace('#', '%23')}`;
    url += `&linestyle=${linestyle}`;
    url += `&marker=${marker}`;

    return this.http.get(url, {responseType: 'blob'});
  }

  getBoxplot(x, y, selected, yMin, yMax): Observable<Blob> {
    let url = `${this.boxplotUrl}?`;
    url += `x=${x}&y=${y}`;
    url += `&categories=${encodeURIComponent(JSON.stringify(selected))}`;
    url += `&ymin=${yMin}&ymax=${yMax}`;

    return this.http.get(url, {responseType: 'blob'});
  }

  getInfos(col) {
    return this.http.get(`${this.infosUrl}?col=${col}`);
  }

  getCategories(x) {
    return this.http.get(`${this.categoriesUrl}?x=${x}`);
  }

  postStyle(style: any) {
    const formData = new FormData();
    formData.append('style', style);

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post<any>(`${this.styleUrl}`, style, options);
  }
}
