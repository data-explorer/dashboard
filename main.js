const { app, BrowserWindow, ipcMain } = require('electron');
var path = require('path');

let win;
let maximized = false; // Need to use this variable as win.isMaximized() doesn't work as expected.

function createWindow () {
  win = new BrowserWindow({
    width: 1366,
    height: 768,
    transparent: true,
    frame: false,
    backgroundColor: '#00ffffff',
    icon: path.join(__dirname, '/src/assets/img/icon.png'),
    webPreferences: {
      nodeIntegration: true,
      experimentalFeatures: true
    }
  });

  win.loadFile('./dist/dashboard/index.html');
  win.webContents.executeJavaScript('document.body.classList.add("electron")');
  win.webContents.openDevTools();

  win.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
  })
}

app.on('ready', () => {
  setTimeout(() => {
    createWindow();
  }, 1000);
})

app.on('window-all-closed', () => { // Quit when all windows are closed.
  if (process.platform !== 'darwin') {
    app.quit();
  }
})

app.on('activate', () => {
  if (win === null) {
    createWindow();
  }
})

ipcMain.on('closeApp', () => {
  console.log('App closed.');
  app.quit();
})

ipcMain.on('maximizeApp', () => {
  if (maximized) {
    win.unmaximize();
    win.webContents.executeJavaScript('document.body.classList.remove("maximized")');
  } else {
    win.maximize();
    win.webContents.executeJavaScript('document.body.classList.add("maximized")');
  }
  maximized = !maximized;
})

ipcMain.on('minimizeApp', () => {
  if (!win.isMinimized()) {
    win.minimize();
  }
})
