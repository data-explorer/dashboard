# Data Absolver frontend

This project is the frontend for the app Data Absolver.
It is an Angular frontend.

## Prerequisites 

  * Node.js
  * npm
  * Angular 7 CLI

## Install the dependencies

```
npm install
```

## Run the project

The app can be run within a browser, or in a native window (based on electron npm module). 

### Run in browser 

```
ng serve
```
Access the url `http://localhost:4200` on your browser.

### Run in native window

```
npm run electron-build
npm run electron
```

The window will open by itself.

_Notes:_ 
  * _This is currently not working for Linux_
  * _Chrome Dev Tools should either be deactivated in `main.ts`, or separated from the main window in the browser interface_
